#
# Import Python

from datetime import datetime


#
# Import Django
from django.http import FileResponse 
from reportlab.pdfgen import canvas  
from reportlab.lib.units import inch 
from reportlab.lib.pagesizes import letter  
from reportlab.platypus import Table 
from reportlab.lib import colors
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
import plotly.graph_objects as go
import pandas as pd
from plotly.offline import plot
import plotly.express as px
import plotly.offline as pyo
from dash import html
import dash

#
# Import models

from .models import Project, Task, Entry
from apps.team.models import Team



#
# View

@login_required
def projects(request):
    team = get_object_or_404(Team, pk=request.user.userprofile.active_team_id, status=Team.ACTIVE)
    projects = team.projects.all()

    if request.method == 'POST':
        title = request.POST.get('title')

        if title:
            project = Project.objects.create(team=team, title=title, created_by=request.user)

            messages.info(request, 'The project was added!')

            return redirect('project:projects')

    return render(request, 'project/projects.html', {'team': team, 'projects': projects})

@login_required
def project(request, project_id):
    team = get_object_or_404(Team, pk=request.user.userprofile.active_team_id, status=Team.ACTIVE)
    project = get_object_or_404(Project, team=team, pk=project_id)

    if request.method == 'POST':
        title = request.POST.get('title')
        task_startdate = request.POST.get('task_startdate')
        task_enddate = request.POST.get('task_enddate')
        file = request.POST.get('file')

        if title:
            task = Task.objects.create(team=team, project=project, created_by=request.user, title=title, task_startdate=task_startdate, task_enddate=task_enddate, file=file)

            messages.info(request, 'The task was added!')

            return redirect('project:project', project_id=project.id)
    
    tasks_Todo = project.tasks.filter(status=Task.Todo)
    tasks_done = project.tasks.filter(status=Task.DONE)

    return render(request, 'project/project.html', {'team': team, 'project': project, 'tasks_Todo': tasks_Todo, 'tasks_done': tasks_done})

@login_required
def edit_project(request, project_id):
    team = get_object_or_404(Team, pk=request.user.userprofile.active_team_id, status=Team.ACTIVE)
    project = get_object_or_404(Project, team=team, pk=project_id)

    if request.method == 'POST':
        title = request.POST.get('title')

        if title:
            project.title = title
            project.save()

            messages.info(request, 'Амжилттай хадгаллаа')

            return redirect('project:project', project_id=project.id)
    
    return render(request, 'project/edit_project.html', {'team': team, 'project': project})

@login_required
def task(request, project_id, task_id):
    team = get_object_or_404(Team, pk=request.user.userprofile.active_team_id, status=Team.ACTIVE)
    project = get_object_or_404(Project, team=team, pk=project_id)
    task = get_object_or_404(Task, pk=task_id, team=team)

    if request.method == 'POST':
        hours = int(request.POST.get('hours', 0))
        minutes = int(request.POST.get('minutes', 0))
        date = '%s %s' % (request.POST.get('date'), datetime.now().time())
        minutes_total = (hours * 60) + minutes
        entry = Entry.objects.create(team=team, project=project, task=task, minutes=minutes_total, created_by=request.user, created_at=date, is_tracked=True)

       
    return render(request, 'project/task.html', {'today': datetime.today(), 'team': team, 'project': project, 'task': task})

@login_required
def edit_task(request, project_id, task_id):
    team = get_object_or_404(Team, pk=request.user.userprofile.active_team_id, status=Team.ACTIVE)
    project = get_object_or_404(Project, team=team, pk=project_id)
    task = get_object_or_404(Task, pk=task_id, team=team)

    if request.method == 'POST':
        title = request.POST.get('title')
        status = request.POST.get('status')
        task_startdate = request.POST.get('task_startdate')
        task_enddate = request.POST.get('task_enddate')

        if title:
            task.title = title
            task.status = status
            task.task_startdate = task_startdate
            task.task_enddate = task_enddate
            task.save()

            messages.info(request, 'The changes was saved!')

            return redirect('project:task', project_id=project.id, task_id=task.id)

    return render(request, 'project/edit_task.html', {'team': team, 'project': project, 'task': task})

@login_required
def edit_entry(request, project_id, task_id, entry_id):
    team = get_object_or_404(Team, pk=request.user.userprofile.active_team_id, status=Team.ACTIVE)
    project = get_object_or_404(Project, team=team, pk=project_id)
    task = get_object_or_404(Task, pk=task_id, team=team)
    entry = get_object_or_404(Entry, pk=entry_id, team=team)

    if request.method == 'POST':
        hours = int(request.POST.get('hours', 0))
        minutes = int(request.POST.get('minutes', 0))

        date = '%s %s' % (request.POST.get('date'), datetime.now().time())

        entry.created_at = date
        entry.minutes = (hours * 60) + minutes
        entry.save()

        messages.info(request, 'The changes was saved!')

        return redirect('project:task', project_id=project.id, task_id=task.id)
    
    hours, minutes = divmod(entry.minutes, 60)
    
    context = {
        'team': team,
        'project': project,
        'task': task,
        'entry': entry,
        'hours': hours,
        'minutes': minutes
    }
    
    return render(request, 'project/edit_entry.html', context)

@login_required
def delete_entry(request, project_id, task_id, entry_id):
    team = get_object_or_404(Team, pk=request.user.userprofile.active_team_id, status=Team.ACTIVE)
    project = get_object_or_404(Project, team=team, pk=project_id)
    task = get_object_or_404(Task, pk=task_id, team=team)
    entry = get_object_or_404(Entry, pk=entry_id, team=team)
    entry.delete()

    messages.info(request, 'The entry was deleted!')

    return redirect('project:task', project_id=project.id, task_id=task.id)

@login_required
def delete_untracked_entry(request, entry_id):
    team = get_object_or_404(Team, pk=request.user.userprofile.active_team_id, status=Team.ACTIVE)
    entry = get_object_or_404(Entry, pk=entry_id, team=team)
    entry.delete()

    messages.info(request, 'The entry was deleted!')

    return redirect('dashboard')

@login_required
def track_entry(request, entry_id):
    team = get_object_or_404(Team, pk=request.user.userprofile.active_team_id, status=Team.ACTIVE)
    entry = get_object_or_404(Entry, pk=entry_id, team=team)
    projects = team.projects.all()

    if request.method == 'POST':
        hours = int(request.POST.get('hours', 0))
        minutes = int(request.POST.get('minutes', 0))
        project = request.POST.get('project')
        task = request.POST.get('task')

        if project and task:
            entry.project_id = project
            entry.task_id = task
            entry.minutes = (hours * 60) + minutes
            entry.created_at = '%s %s' % (request.POST.get('date'), entry.created_at.time())
            entry.is_tracked = True
            entry.save()

            messages.info(request, 'The time was tracked')

            return redirect('dashboard')
    
    hours, minutes = divmod(entry.minutes, 60)

    context = {
        'hours': hours,
        'minutes': minutes,
        'team': team,
        'projects': projects,
        'entry': entry
    }

    return render(request, 'project/track_entry.html', context)


def Charts(request, id):

    proj = Project.objects.get(id = id)
    tasks = Task.objects.filter(project = proj)
    fig = go.Figure(data=go.Scatter(x=[1, 3, 2], y=[4,5,6]))
    projects_data = [
        {
            'Task': id,
            'Start': x.task_startdate,
            'Finish': x.task_enddate,
            'Task': x.title
        } for x in tasks
    ]
    df = pd.DataFrame(projects_data)
    fig = px.timeline(
        df, x_start="Start", x_end="Finish", y="Task", color="Task", width=1000, height=400
    )
    layout = {
        "margin": {"l": 1, "r": 1, "t": 50, "b": 50},
        "width": 1000,
        "height": 500,
        "autosize": False,
        "paper_bgcolor": "rgba(0,0,0,0)",
    }

    fig.update_layout(
        layout,
        yaxis=dict(showgrid=True),
        xaxis=dict(showgrid=False),
    )
    fig.update_yaxes(autorange="reversed")
    fig.update_traces(width=0.2)
    gantt_plot = plot(fig, output_type="div")
    context = {'plot_div': gantt_plot, 'task': tasks }
    return render(request, 'Gantt/base.html', context)

def Pdf(request, project_id, task_id):
    team = get_object_or_404(Team, pk=request.user.userprofile.active_team_id, status=Team.ACTIVE)
    project = get_object_or_404(Project, team=team, pk=project_id)
    task = get_object_or_404(Task, pk=task_id, team=team)

    task_id = request.GET.get('task_id').strip()

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="taskData.pdf"'


    image_path = 'static/images/index.jpg' 
    result = Task.objects.filter(id=task_id)

    for fields in result:
         team = fields.team
         project = fields.project
         title = fields.title                                                                                                        
         created_by = fields.created_by
         created_at = fields.created_at
         task_startdate = fields.task_startdate
         task_enddate = fields.task_enddate
         status = fields.status


    p = canvas.Canvas(response, pagesize=letter)
    p.drawImage(image_path, 30, 8.5 * inch, width=7.5 * inch, height=2.2 * inch )   
    p.setFont("Times-Bold", 24)
    p.drawString(30, 570, 'Task data')   
    p.setFont("Helvetica", 14) 
    p.drawString(30, 545, "Task title:" + ' ' + title) 

    data = [
        ['FIELD', 'INFO'],
        ['Team', team],
        ['Project', project],
        ['Title', title],
        ['Created by', created_by],
        ['Created at', created_at],
        ['Task startdate', task_startdate],
        ['Task enddate', task_enddate],
        ['Status', status]
    ]
    
    table = Table(data)
    table.setStyle([
        ('BACKGROUND', (0, 0), (-1, 0), colors.blueviolet),
        ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
        ('FONTSIZE', (0, 0), (-1, 0), 16),
        ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
        ('BACKGROUND', (0, 1), (-1, -1), colors.white),
        ('GRID', (0, 0), (-1, -1), 1, colors.black),
    ])
    table.wrapOn(p, 4 * inch, 5 * inch)
    table.drawOn(p, 30, 3.5 * inch) 

    p.showPage()
    p.save()

    return response
